package situacion2;

import java.util.ArrayList;

public class Empresa {
    private String nombre;
    private String ubicacion;
    private ArrayList<OrdenDeCompra> ordenesCompra;
    private ArrayList <OrdenDeVenta> ordenesVenta;

    public Empresa(String nombre, String ubicacion) {
        this.nombre = nombre;
        this.ubicacion = ubicacion;
        ordenesVenta= new ArrayList <OrdenDeVenta>();
        ordenesCompra= new ArrayList <OrdenDeCompra>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUbicacion() {
        return ubicacion;
    }

    public void setUbicacion(String ubicacion) {
        this.ubicacion = ubicacion;
    }

    public ArrayList<OrdenDeVenta> getOrdenesVenta() {
        return ordenesVenta;
    }

    public void agregarOrdenesDeVenta(OrdenDeVenta ordenDeVenta) {
        ordenesVenta.add(ordenDeVenta);
    }

    public ArrayList<OrdenDeCompra> getOrdenesCompra() {
        return ordenesCompra;
    }

    public void agregarOrdenesDeCompra(OrdenDeCompra ordenDeCompra) {
        ordenesCompra.add(ordenDeCompra);
    }

    public void listarVenta(){
        System.out.println("//////////PRODUCTOS VENDIDOS ///////////");
        for(OrdenDeVenta var:ordenesVenta){
            var.mostrarItems();
            System.out.println("-------------------------------------------------------");
            System.out.println("Total precio de venta: "+var.getTotalPrecioVenta());
            System.out.println("-----------------------------------------------------------");
        }
    }

    public void listarCompra(){
        System.out.println("//////////PRODUCTOS COMPRADOS///////////");
        for(OrdenDeCompra var:ordenesCompra){
            var.mostrarMateriasPrimas();
            System.out.println("-------------------------------------------------------");
            var.getProveedor().listarDatos();
            System.out.println("-------------------------------------------------------");
            System.out.println("Total precio de compra: "+var.getTotalPrecioCompra());
        }
    }
    
}
