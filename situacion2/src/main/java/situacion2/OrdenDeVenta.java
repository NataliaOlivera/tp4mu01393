package situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;

public class OrdenDeVenta {
    private Empresa empresa;
    private ArrayList<Item> items;
    private BigDecimal totalPrecioVenta;

    public OrdenDeVenta() {
        items= new ArrayList<Item>();
    }

    public Empresa getEmpresa() {
        return empresa;
    }

    public void setEmpresa(Empresa empresa) {
        this.empresa = empresa;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(Item item) {
        items.add(item);
        setTotalPrecioVenta();
    }

    public BigDecimal getTotalPrecioVenta() {
        return totalPrecioVenta;
    }

    public void setTotalPrecioVenta() {
        totalPrecioVenta=new BigDecimal(0);
        for(Item var: items){
            totalPrecioVenta= totalPrecioVenta.add(var.getPrecio());
        }
    }

    public void mostrarItems(){
        for(Item var:items){
            var.listarDatos();
        }
    }
    
}
