package situacion2;

import java.math.BigDecimal;
import java.util.ArrayList;

public class OrdenDeCompra {
    private Proveedor proveedor;
    private ArrayList<MateriaPrima> materiasPrimas;
    private BigDecimal totalPrecioCompra;

    public OrdenDeCompra(){
        materiasPrimas= new ArrayList<MateriaPrima>();
    }

    public Proveedor getProveedor() {
        return proveedor;
    }

    public void setProveedor(Proveedor proveedor) {
        this.proveedor = proveedor;
    }

    public ArrayList<MateriaPrima> getMateriasPrimas() {
        return materiasPrimas;
    }

    public void setMateriasPrimas(MateriaPrima materiaPrima) {
        materiasPrimas.add(materiaPrima);
        setTotalPrecioCompra();
    }

    public void setTotalPrecioCompra() {
        totalPrecioCompra=new BigDecimal(0);
	    for(MateriaPrima var: materiasPrimas){
            totalPrecioCompra=totalPrecioCompra.add(var.getPrecio());
        }
    }

    public BigDecimal getTotalPrecioCompra() {
        return totalPrecioCompra;
    }

    public void mostrarMateriasPrimas(){
        for(MateriaPrima var: materiasPrimas){
            var.listarDatos();
            System.out.println("-------------------------------------------------------");
        }
    }
}
