package situacion2;

public class Proveedor {
    private String nombre;
    private Integer cuit;
    private OrdenDeCompra ordenDeCompra;

    public Proveedor(String nombre, Integer cuit) {
        this.nombre = nombre;
        this.cuit = cuit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCuit() {
        return cuit;
    }

    public void setCuit(Integer cuit) {
        this.cuit = cuit;
    }

    public OrdenDeCompra getOrdenDeCompra() {
        return ordenDeCompra;
    }

    public void listarDatos(){
        System.out.println("//Datos Proveedor//");
        System.out.println("Nombre: "+getNombre());
        System.out.println("Cuit: "+getCuit());
    }
  
}
