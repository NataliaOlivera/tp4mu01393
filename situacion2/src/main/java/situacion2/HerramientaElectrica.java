package situacion2;

import java.math.BigDecimal;

public class HerramientaElectrica extends Herramienta{
    private BigDecimal consumo;

    public HerramientaElectrica(Integer codigo, String nombre, BigDecimal precio, String funcionalidad,
            BigDecimal consumo) {
        super(codigo, nombre, precio, funcionalidad);
        this.consumo = consumo;
    }

    public BigDecimal getConsumo() {
        return consumo;
    }

    public void setConsumo(BigDecimal consumo) {
        this.consumo = consumo;
    }

    @Override
    public void listarDatos(){
        System.out.println("Codigo: "+getCodigo());
        System.out.println("Nombre: "+getNombre());
        System.out.println("Precio: "+getPrecio());
        System.out.println("Funcionalidad: "+getFuncionalidad());
        System.out.println("Consumo: "+getConsumo());
        System.out.println("-------------------------------------------------------");
    }
    
}
