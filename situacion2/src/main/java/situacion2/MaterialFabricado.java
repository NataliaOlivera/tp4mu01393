package situacion2;

import java.math.BigDecimal;

public class MaterialFabricado extends Item {

    public MaterialFabricado(Integer codigo, String nombre, BigDecimal bigDecimal) {
        super(codigo, nombre, bigDecimal);
    }

    @Override
    public void listarDatos(){
        System.out.println("Codigo: "+getCodigo());
        System.out.println("Nombre: "+getNombre());
        System.out.println("Precio: "+getPrecio());
        System.out.println("-------------------------------------------------------");
    }
    
}
