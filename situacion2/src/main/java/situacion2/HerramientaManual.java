package situacion2;

import java.math.BigDecimal;

public class HerramientaManual extends Herramienta {

	public HerramientaManual(Integer codigo, String nombre, BigDecimal precio, String funcionalidad) {
		super(codigo, nombre, precio, funcionalidad);
	}

	@Override
    public void listarDatos(){
        System.out.println("Codigo: "+getCodigo());
        System.out.println("Nombre: "+getNombre());
        System.out.println("Precio: "+getPrecio());
        System.out.println("Funcionalidad: "+getFuncionalidad());
        System.out.println("-------------------------------------------------------");
    }
    
}
