package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

public class EmpresaTest {

    @Test
    public void agregarOrdenesDeVenta(){
        Empresa empresa= new Empresa("La Empresa", "Av. Belgrano");
        OrdenDeVenta venta1= new OrdenDeVenta();
        OrdenDeVenta venta2= new OrdenDeVenta();

        empresa.agregarOrdenesDeVenta(venta1);
        empresa.agregarOrdenesDeVenta(venta2);

        assertEquals(2,empresa.getOrdenesVenta().size());
    }

    @Test
    public void agregarOrdenesDeCompra(){
        Empresa empresa= new Empresa("La Empresa", "Av. Belgrano");
        OrdenDeCompra compra1= new OrdenDeCompra();
        OrdenDeCompra compra2= new OrdenDeCompra();

        empresa.agregarOrdenesDeCompra(compra1);
        empresa.agregarOrdenesDeCompra(compra2);

        assertEquals(2,empresa.getOrdenesCompra().size());
    }
    
}
