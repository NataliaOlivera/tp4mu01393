package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;

public class OrdenDeVentaTest {
    
    @Test
    public void agregarItemsParaLaVenta(){
        OrdenDeVenta ordenDeVenta= new OrdenDeVenta();
        Item materialFabricado= new MaterialFabricado(3444, "Ladrillo", new BigDecimal(30));
        Item herramientaManual= new HerramientaManual(234, "Martillo", new BigDecimal (100), "Martillar");
        Item herramientaElectrica= new HerramientaElectrica(675, "Taladro", new BigDecimal(500), "Taladrar", new BigDecimal(45));

        ordenDeVenta.setItems(materialFabricado);
        ordenDeVenta.setItems(herramientaManual);
        ordenDeVenta.setItems(herramientaElectrica);

        assertEquals(3,ordenDeVenta.getItems().size());
    }


    @Test
    public void calcularTotalPrecioVenta(){
        OrdenDeVenta ordenDeVenta= new OrdenDeVenta();
        Item materialFabricado= new MaterialFabricado(3444, "Ladrillo", new BigDecimal(30));
        Item herramientaManual= new HerramientaManual(234, "Martillo", new BigDecimal (100), "Martillar");
        Item herramientaElectrica= new HerramientaElectrica(675, "Taladro", new BigDecimal(500), "Taladrar", new BigDecimal(45));

        ordenDeVenta.setItems(materialFabricado);
        ordenDeVenta.setItems(herramientaManual);
        ordenDeVenta.setItems(herramientaElectrica);

        BigDecimal totalEsperado= ordenDeVenta.getTotalPrecioVenta();

        assertEquals(new BigDecimal(630), totalEsperado);
    }
}
