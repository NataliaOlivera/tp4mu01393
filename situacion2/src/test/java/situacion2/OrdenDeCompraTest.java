package situacion2;

import org.junit.Test;
import static org.junit.Assert.*;

import java.math.BigDecimal;

public class OrdenDeCompraTest {

    @Test
    public void agregarMaterialParaLaCompra(){
        OrdenDeCompra ordenDeCompra= new OrdenDeCompra();

        MateriaPrima materia1= new MateriaPrima(567, "Cal", new BigDecimal(200));
        MateriaPrima materia2= new MateriaPrima(455, "Camento", new BigDecimal(300));

        ordenDeCompra.setMateriasPrimas(materia1);
        ordenDeCompra.setMateriasPrimas(materia2);      

        assertEquals(2,ordenDeCompra.getMateriasPrimas().size());
    }


    @Test
    public void calcularTotalPrecioCompra(){
        OrdenDeCompra ordenDeCompra= new OrdenDeCompra();

        MateriaPrima materia1= new MateriaPrima(567, "Cal", new BigDecimal(200));
        MateriaPrima materia2= new MateriaPrima(455, "Camento", new BigDecimal(300));

        ordenDeCompra.setMateriasPrimas(materia1);
        ordenDeCompra.setMateriasPrimas(materia2); 
        
        BigDecimal totalEsperado= ordenDeCompra.getTotalPrecioCompra();

        assertEquals(new BigDecimal(500),totalEsperado);
    }
    
}
