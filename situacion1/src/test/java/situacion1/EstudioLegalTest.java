package situacion1;

import org.junit.Test;
import static org.junit.Assert.*;

public class EstudioLegalTest {

    @Test
    public void agregarClientes(){
        EstudioLegal estudioLegal= new EstudioLegal("Legales", "Av. Virgen del Valle");
        Cliente cliente1=new Cliente("Olivera", "Natalia", 40598438,"Consulta","nati@gmail.com",13344);
        Domicilio domicilio1= new DomicilioEdificio("Calle", 23, 15);
        cliente1.setDomicilio(domicilio1);
        Cliente cliente2=new Cliente("Pereyra", "Evelyn", 322442,"Consulta legal","eve@gmail.com",144);
        Domicilio domicilio2= new DomicilioBarrial("Av Ocampo", "Barrio","Manzana 33" ,56 );
        cliente1.setDomicilio(domicilio2);

        estudioLegal.setCliente(cliente1);
        estudioLegal.setCliente(cliente2);

        assertEquals(2,estudioLegal.getClientes().size());
    }

}
