package situacion1;

import java.util.ArrayList;

public class EstudioLegal {
    private String nombre;
    private String direccion;
    private ArrayList<Cliente> clientes;

    public EstudioLegal(String nombre, String direccion) {
        this.nombre = nombre;
        this.direccion = direccion;
        clientes= new ArrayList<Cliente>();
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public ArrayList<Cliente> getClientes() {
        return clientes;
    }

    public void setCliente(Cliente cliente) {
        clientes.add(cliente);
    }

    public void listarClientes(){
        System.out.println("///////////CLIENTES///////////");
        for(Cliente var: clientes){
            System.out.println("Apellido: "+var.getApellido());
            System.out.println("Nombre: "+var.getNombre());
            System.out.println("Dni: "+var.getDni());
            System.out.println("Asunto: "+var.getAsunto());
            System.out.println("Correo Electronico: "+var.getCorreoElectronico());
            System.out.println("Telefono: "+var.getTelefono());
            var.getDomicilio().listarDatos();  
            System.out.println("------------------------------------------------------");
        }
    }

}