package situacion1;

public class DomicilioEdificio extends Domicilio{
    private Integer piso;
    private Integer departamento;

    public DomicilioEdificio(String calle, Integer piso, Integer departamento) {
        super(calle);
        this.piso = piso;
        this.departamento = departamento;
    }

    public Integer getPiso() {
        return piso;
    }

    public void setPiso(Integer piso) {
        this.piso = piso;
    }

    public Integer getDepartamento() {
        return departamento;
    }

    public void setDepartamento(Integer departamento) {
        this.departamento = departamento;
    }

    @Override
    public void listarDatos(){
        System.out.println("Datos Domicilio: ");
         System.out.println("Calle: "+getCalle());
         System.out.println("Piso: "+getPiso());
         System.out.println("Departamento: "+getDepartamento());
    }
    
}
