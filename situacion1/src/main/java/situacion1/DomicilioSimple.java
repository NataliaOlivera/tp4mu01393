package situacion1;

public class DomicilioSimple extends Domicilio {
   private Integer numero;

   public DomicilioSimple(String calle, Integer numero) {
       super(calle);
       this.numero = numero;
   }

   public Integer getNumero() {
       return numero;
   }

   public void setNumero(Integer numero) {
       this.numero = numero;
   }

   @Override
   public void listarDatos(){
       System.out.println("Datos Domicilio: ");
        System.out.println("Calle: "+getCalle());
        System.out.println("Numero: "+getNumero());
   }
  
}
