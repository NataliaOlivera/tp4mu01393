package situacion1;

public class Cliente {
    private String apellido;
    private String nombre;
    private Integer dni;
    private String asunto;
    private String correoElectronico;
    private Integer telefono;
    private EstudioLegal estudioLegal;
    private Domicilio domicilio;

    public Cliente(String apellido, String nombre, Integer dni, String asunto, String correoElectronico,
            Integer telefono) {
        this.apellido = apellido;
        this.nombre = nombre;
        this.dni = dni;
        this.asunto = asunto;
        this.correoElectronico = correoElectronico;
        this.telefono = telefono;
        setDomicilio(domicilio);
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getDni() {
        return dni;
    }

    public void setDni(Integer dni) {
        this.dni = dni;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getCorreoElectronico() {
        return correoElectronico;
    }

    public void setCorreoElectronico(String correoElectronico) {
        this.correoElectronico = correoElectronico;
    }

    public Integer getTelefono() {
        return telefono;
    }

    public void setTelefono(Integer telefono) {
        this.telefono = telefono;
    }

    public EstudioLegal getEstudioLegal() {
        return estudioLegal;
    }

    public Domicilio getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(Domicilio domicilio) {
        this.domicilio = domicilio;
    }

}
