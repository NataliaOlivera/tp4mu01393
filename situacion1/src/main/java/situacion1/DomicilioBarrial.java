package situacion1;

public class DomicilioBarrial extends Domicilio {
    private String barrio;
    private String manzana;
    private Integer numeroCasa;

    public DomicilioBarrial(String calle, String barrio, String manzana, Integer numeroCasa) {
        super(calle);
        this.barrio = barrio;
        this.manzana = manzana;
        this.numeroCasa = numeroCasa;
    }

    public String getBarrio() {
        return barrio;
    }

    public void setBarrio(String barrio) {
        this.barrio = barrio;
    }

    public String getManzana() {
        return manzana;
    }

    public void setManzana(String manzana) {
        this.manzana = manzana;
    }

    public Integer getNumeroCasa() {
        return numeroCasa;
    }

    public void setNumeroCasa(Integer numeroCasa) {
        this.numeroCasa = numeroCasa;
    }

    @Override
    public void listarDatos(){
        System.out.println("Datos Domicilio: ");
         System.out.println("Calle: "+getCalle());
         System.out.println("Barrio: "+getBarrio());
         System.out.println("Manzana: "+getManzana());
         System.out.println("Numero de casa: "+getNumeroCasa());
    }
}
